package com.curso.integracion.aplicaciones.apache.camel.routes;

import com.curso.integracion.aplicaciones.apache.camel.entity.Users;
import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.net.ConnectException;
import java.sql.SQLNonTransientConnectionException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.sql.DataSource;

@Service
public class UserService extends RouteBuilder{
  
  @Autowired
  DataSource datasource;
  
  @Override
  public void configure() throws Exception {

    /*configuracion para el control de excepciones que a diferencia del errorHandel se le puede especificcar que tipo de exceciones va 
     * a considerar, se le puede agregar un numero de reintentos, asi como la politica o reglas para ejecutar esos reintentos y un proceso
     * a ejecutar despues de cierta excepcion.*/
    onException(NullPointerException.class,SQLNonTransientConnectionException.class)
      .process((ex)->{System.out.println("handling ex");})
        .log("Received body")
      .handled(true)
      //.maximumRedeliveries(3)
       .transform().simple("Error reported: ${exception.message} - cannot process this message.").end();
    
    ;
   /*funcion definida por apache camel para el control de excepciones */
   errorHandler(defaultErrorHandler());
   /*ruta para realizar un busqueda en base de datos mediante el componente jdbc*/
    from("direct:findAll")
      .setBody(constant("select * from integrator.users"))
        .to("jdbc:datasource")
          .process(this::getSqlData);
    /*ruta para realizar un insert en base de datos mediante el componente jdbc*/
    from("direct:insert")
     .process(this::saveSqlData)
       .to("jdbc:dataSource");
    /*ruta para realizar un busqueda por Id en base de datos mediante el componente jdbc*/
    from("direct:findById")
    .process(this::querybuilder)
      .to("jdbc:dataSource")
         .process(this::getSqlData);
    
    /*ruta para realizar un busqueda en base de datos mediante el componente sql*/
    from("direct:selectQL")
      //,split()
      .to("sql:select * from integrator.users")
        .process(this::getSqlData);
    
    
    
  }
  
  @SuppressWarnings("unchecked")
  private void getSqlData(Exchange exchange) throws Exception {
    ArrayList<Map<String, String>> dataList = (ArrayList<Map<String, String>>) exchange.getIn()
        .getBody();
    List<Users> usersList = new ArrayList<Users>();
    usersList =  dataList.stream()
                         .map(m-> new Users(m.get("userId"),m.get("userName")))
                         .collect(Collectors.toList());
    exchange.getIn().setBody(usersList);
    
  }
  
  private void saveSqlData(Exchange exchange) throws Exception {
    Users user = exchange.getIn().getBody(Users.class);
    String query = "INSERT INTO users(userId,userName)values('" + user.getUserId() + "','"
            + user.getUserName() + "')";
    exchange.getIn().setBody(query);
  }
  
private void querybuilder(Exchange exchange) throws Exception {
    String query = "select * from integrator.users where userId = " + exchange.getIn().getBody(Integer.class);
    exchange.getIn().setBody(query);
  }


}
